@echo off

rmdir /s /q "%HOMEDRIVE%%HOMEPATH%\AppData\Local\Programs\Simular-win32-x64"
7z-extra\7za x Simular-win32-x64.zip -o"%HOMEDRIVE%%HOMEPATH%\AppData\Local\Programs"

echo Set WS = WScript.CreateObject("WScript.Shell") > CreateSimularShortcut.vbs
echo SimularLinkFile = "%HOMEDRIVE%%HOMEPATH%\Desktop\Simular.lnk" >> CreateSimularShortcut.vbs
echo Set SimularLinkObject = WS.CreateShortcut(SimularLinkFile) >> CreateSimularShortcut.vbs
echo SimularLinkObject.TargetPath = "%HOMEDRIVE%%HOMEPATH%\AppData\Local\Programs\Simular-win32-x64\Simular.exe" >> CreateSimularShortcut.vbs
echo SimularLinkObject.Arguments = "--js-flags=""--max_old_space_size=8192""" >> CreateSimularShortcut.vbs
echo SimularLinkObject.IconLocation = "%HOMEDRIVE%%HOMEPATH%\AppData\Local\Programs\Simular-win32-x64\Simular.exe" >> CreateSimularShortcut.vbs
echo SimularLinkObject.WindowStyle = 3 >> CreateSimularShortcut.vbs
echo SimularLinkObject.WorkingDirectory = "%HOMEDRIVE%%HOMEPATH%\Documents" >> CreateSimularShortcut.vbs
echo SimularLinkObject.Save >> CreateSimularShortcut.vbs
cscript CreateSimularShortcut.vbs
del CreateSimularShortcut.vbs

echo Set WS = WScript.CreateObject("WScript.Shell") > CreateAtualizarShortcut.vbs
echo AtualizarLinkFile = "%HOMEDRIVE%%HOMEPATH%\Desktop\Atualizar Simular.lnk" >> CreateAtualizarShortcut.vbs
echo Set AtualizarLinkObject = WS.CreateShortcut(AtualizarLinkFile) >> CreateAtualizarShortcut.vbs
echo AtualizarLinkObject.TargetPath = "V:\UO-SEAL\NP-2\Reservatorio\Interno\SEAL Data Labs\install-simular.cmd" >> CreateAtualizarShortcut.vbs
echo AtualizarLinkObject.Description = "Atualizar Simular" >> CreateAtualizarShortcut.vbs
echo AtualizarLinkObject.WorkingDirectory = "V:\UO-SEAL\NP-2\Reservatorio\Interno\SEAL Data Labs" >> CreateAtualizarShortcut.vbs
echo AtualizarLinkObject.Save >> CreateAtualizarShortcut.vbs
cscript CreateAtualizarShortcut.vbs
del CreateAtualizarShortcut.vbs
