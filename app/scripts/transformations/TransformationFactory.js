var smlr = smlr || {};

smlr.TransformationFactory = function() {};

smlr.TransformationFactory.prototype.create = function(functionId) {
  var f = this[functionId];

  if (!f) {
    throw new Error('Transformation "' + functionId + '" does not exists');
  }

  return f();
};

smlr.TransformationFactory.prototype._names = {};

smlr.TransformationFactory.prototype.nameOf = function(id) {
  return smlr.TransformationFactory.prototype._names[id];
};

smlr.TransformationFactory.prototype.register = function(ctor) {
  smlr.TransformationFactory.prototype._names[ctor.prototype.id] = ctor.prototype.name;
  smlr.TransformationFactory.prototype[ctor.prototype.id] = function() {
    return new ctor();
  };
};
