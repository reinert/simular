smlr.MinMaxNormalization = function() {};

smlr.MinMaxNormalization.prototype = Object.create(smlr.AbstractTransformation.prototype);

smlr.MinMaxNormalization.prototype.constructor = smlr.MinMaxNormalization;

smlr.MinMaxNormalization.prototype.id = 'mmn';

smlr.MinMaxNormalization.prototype.name = 'Normalização Min-Max';

smlr.MinMaxNormalization.prototype.execute = function(params, paramIdx) {
  var paramValues = params.map(function(d) {
    return d[paramIdx];
  });

  paramValues.splice(0, 1);

  var paramValuesFiltered = paramValues.filter(function(v) {
    return !isNaN(v);
  });
  //var max = Math.max(...paramValuesFiltered);
  var max = Math.max.apply(null, paramValuesFiltered);
  ///var min = Math.min(...paramValuesFiltered);
  var min = Math.min.apply(null, paramValuesFiltered);

  return paramValues.map(function(d) {
    return (d - min) / (max - min);
  });
};

smlr.TransformationFactory.prototype.register(smlr.MinMaxNormalization);
