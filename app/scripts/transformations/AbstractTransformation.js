var smlr = smlr || {};

smlr.AbstractTransformation = function() {};

smlr.AbstractTransformation.prototype = {
  constructor: smlr.AbstractTransformation,

  _calculateNewParam: function(params, paramIdx) {
    return 0;
  },

  _getSimParams: function(params, simIdx) {
    var paramsNames = params[0];
    var paramsValues = params[simIdx];
    var obj = {};
    for (var i in paramsNames) {
      obj[paramsNames[i]] = paramsValues[i];
    }
    return obj;
  },

  execute: function(params, paramIdx) {
    return [];
  }
};
