smlr.RemoveFilterCommand = function(props) {
  smlr.AbstractCommand.call(this);

  this.index = props.index;
  this.filter = props.filter;
};

smlr.RemoveFilterCommand.prototype = Object.create(smlr.AbstractCommand.prototype);

smlr.RemoveFilterCommand.prototype.constructor = smlr.RemoveFilterCommand;

Object.defineProperties(smlr.RemoveFilterCommand.prototype, {
  '_id': { value: 'RemoveFilter' },

  '_name': { value: 'Remover filtro' },

  '_description': { get: function() {
      return 'Filtro "' + this.name + '" foi removido da análise.';
  }}
});

smlr.RemoveFilterCommand.prototype._do = function(analysis, resolve, reject) {
  if (this.index == null && this.filter != null) {
    this.index = analysis.filters.indexOf(this.filter);
    if (this.index === -1) {
      console.info(this.filter);
      console.log(analysis.filters);
      reject(new Error('Filter does not exists.'));
    }
    this.filter = null; // clear cache; save memory; avoid serialization problems
  }

  this.__removedFilter = analysis.splice('filters', this.index, 1);

  this._callResolve(resolve);
};

smlr.RemoveFilterCommand.prototype._undo = function(analysis, resolve, reject) {
  if (this.__removedFilter) {
    analysis.splice('filters', this.index, 0, this.__removedFilter);
  }

  this._callResolve(resolve);
};

smlr.CommandFactory.prototype.register(smlr.RemoveFilterCommand);
