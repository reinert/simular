smlr.ApplyTransformationCommand = function(props) {
  smlr.AddParamCommand.call(this, props);

  this.functionId = props.functionId;
  this.inputParamName = props.inputParamName;
};

smlr.ApplyTransformationCommand.prototype = Object.create(smlr.AbstractCommand.prototype);

smlr.ApplyTransformationCommand.prototype.constructor = smlr.ApplyTransformationCommand;

Object.defineProperties(smlr.ApplyTransformationCommand.prototype, {
  '_id': { value: 'ApplyTransformation' },

  '_name': { value: 'Aplicar transformação' },

  '_description': { get: function() {
    return smlr.TransformationFactory.prototype.nameOf(this.functionId)
      + '" foi aplicado(a) ao parâmetro "' + this.paramName + '.';
  }}
});

smlr.ApplyTransformationCommand.prototype._do = function(analysis, resolve, reject) {
  var f = smlr.TransformationFactory.prototype.create(this.functionId);

  var paramIdx = analysis.params[0].indexOf(this.inputParamName);
  this.paramData = f.execute(analysis.params, paramIdx);

  smlr.AddParamCommand.prototype._do.call(this, analysis, resolve, reject); // resolves here

  this.paramData = null; // save memory
};

smlr.ApplyTransformationCommand.prototype._undo = function(analysis, resolve, reject) {
  smlr.AddParamCommand.prototype._undo.call(this, analysis, resolve, reject);
};

smlr.CommandFactory.prototype.register(smlr.ApplyTransformationCommand);
