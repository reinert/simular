smlr.AddPlotCommand = function(props) {
  smlr.AbstractCommand.call(this);

  Object.defineProperties(this, {
    'type': { value: this.type, enumerable: true, writable: false },
    'graph': { enumerable: false, writable: true },
    'position': { enumerable: false, writable: true },
    '_uid': { enumerable: false, writable: true }
  });


  this.logscale = {x: false, y: false};
  this.range = {x: {min: null, max: null}, y: {min: null, max: null}};

  if (props) {
    this.logscale = props.logscale || this.logscale;
    this.range = props.range || this.range;
  }
};

smlr.AddPlotCommand.prototype = Object.create(smlr.AbstractCommand.prototype);

smlr.AddPlotCommand.prototype.constructor = smlr.AddPlotCommand;

Object.defineProperties(smlr.AddPlotCommand.prototype, {
  '_id': { value: 'AddPlot' },

  '_name': { value: 'Adicionar gráfico' },

  '_description': { get: function() {
    return 'Gráfico adicionado à análise.';
  }},

  'model': { get: function() {
    // empty object that will receive copies of properties
    var clone = {};

    var i, keys = Object.keys(this);

    for (i = 0; i < keys.length; i++) {
      // copy each property into the clone
      if (keys[i] !== '_id') clone[keys[i]] = this[keys[i]];
    }

    Object.defineProperties(clone, {
      'type': { value: this.type, enumerable: true, writable: false },
      'graph': { enumerable: false, writable: true },
      'position': { enumerable: false, writable: true },
      '_uid': { enumerable: false, writable: true }
    });

    return clone;
  }}
});

smlr.AddPlotCommand.prototype._do = function(analysis, resolve, reject) {
  analysis.push('plots', this.model);

  this._callResolve(resolve);
};

smlr.AddPlotCommand.prototype._undo = function(analysis, resolve, reject) {
  analysis.pop('plots');

  this._callResolve(resolve);
};

smlr.CommandFactory.prototype.register(smlr.AddPlotCommand);
