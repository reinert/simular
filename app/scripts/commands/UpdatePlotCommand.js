smlr.UpdatePlotCommand = function(props) {
  smlr.AbstractCommand.call(this);

  this.index = props.index;
  this.plot = props.plot;
  this.patch = props.patch;
};

smlr.UpdatePlotCommand.prototype = Object.create(smlr.AbstractCommand.prototype);

smlr.UpdatePlotCommand.prototype.constructor = smlr.UpdatePlotCommand;

Object.defineProperties(smlr.UpdatePlotCommand.prototype, {
  '_id': { value: 'UpdatePlot' },

  '_name': { value: 'Modificar gráfico' },

  '_description': { get: function() {
    return 'Gráfico modificado.';
  }}
});

smlr.UpdatePlotCommand.prototype._do = function(analysis, resolve, reject) {
  if (this.index == null && this.plot != null) {
    this.index = analysis.plots.indexOf(this.plot);
    if (this.index === -1) {
      console.info(this.plot);
      console.log(analysis.plots);
      reject(new Error('Plot does not exists.'));
    }
    this.plot = null; // clear cache; save memory; avoid serialization problems
  }

  this.__revertPatch = {};
  for (var p in this.patch) {
    this.__revertPatch[p] = analysis.plots[this.index][p];
    analysis.set(['plots', this.index, p], this.patch[p]);
  }

  this._callResolve(resolve);
};

smlr.UpdatePlotCommand.prototype._undo = function(analysis, resolve, reject) {
  if (this.__revertPatch) {
    for (var p in this.__revertPatch) {
      analysis.set(['plots', this.index, p], this.__revertPatch[p]);
    }
  }

  this._callResolve(resolve);
};

smlr.UpdatePlotCommand.prototype._copy = function(object) {
  return object ? JSON.parse(JSON.stringify(object)) : {};
};

smlr.CommandFactory.prototype.register(smlr.UpdatePlotCommand);
