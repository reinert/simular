smlr.ToggleCutoffCommand = function() {
  smlr.AbstractCommand.call(this);
};

smlr.ToggleCutoffCommand.prototype = Object.create(smlr.AbstractCommand.prototype);

smlr.ToggleCutoffCommand.prototype.constructor = smlr.ToggleCutoffCommand;

Object.defineProperties(smlr.ToggleCutoffCommand.prototype, {
  '_id': { value: 'ToggleCutoff' },

  '_name': { value: 'Alternar visão' },

  '_description': { get: function() {
      return 'Visão ' + (this.__activated ? 'segmentada' : 'geral')
        + ' foi acionada.';
  }}
});

smlr.ToggleCutoffCommand.prototype._do = function(analysis, resolve, reject) {
  analysis.toggleCutoff();

  this.__activated = analysis.cutoff;

  this._callResolve(resolve);
};

smlr.ToggleCutoffCommand.prototype._undo = function(analysis, resolve, reject) {
  analysis.toggleCutoff();

  this._callResolve(resolve);
};

smlr.CommandFactory.prototype.register(smlr.ToggleCutoffCommand);
