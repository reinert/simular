smlr.ApplyFunctionCommand = function(props) {
  smlr.AddParamCommand.call(this, props);

  this.functionId = props.functionId;
  this.seriesName = props.seriesName;
  this.seriesNameB = props.seriesNameB;
  this.tInitial = props.tInitial;
  this.tFinal = props.tFinal;
  this.pA = props.pA;
  this.pB = props.pB;
};

smlr.ApplyFunctionCommand.prototype = Object.create(smlr.AbstractCommand.prototype);

smlr.ApplyFunctionCommand.prototype.constructor = smlr.ApplyFunctionCommand;

Object.defineProperties(smlr.ApplyFunctionCommand.prototype, {
  '_id': { value: 'ApplyFunction' },

  '_name': { value: 'Aplicar função' },

  '_description': { get: function() {
    if (this.seriesName)
      return 'Função "'
        + smlr.FunctionFactory.prototype.nameOf(this.functionId)
        + '" foi aplicada à série "' + this.seriesName + '"'
        + (this.tInitial || this.tFinal
          ? (' no intervalo ' + (this.tInitial || '0') + ' a ' + (this.tFinal || 'n') + '.')
          : '.');

    return 'Função "'
      + smlr.FunctionFactory.prototype.nameOf(this.functionId)
      + '" foi aplicada aos parâmetros "' + this.pA + '" e "' + this.pB + '".';
  }}
});

smlr.ApplyFunctionCommand.prototype._do = function(analysis, resolve, reject) {
  var f = smlr.FunctionFactory.prototype.create(this.functionId);

  this.paramData = f.execute(analysis.params, analysis.seriesTranspose[this.seriesName], this.tInitial, this.tFinal, this.pA, this.pB, analysis.seriesTranspose[this.seriesNameB]);

  smlr.AddParamCommand.prototype._do.call(this, analysis, resolve, reject); // resolves here

  this.paramData = null; // save memory
};

smlr.ApplyFunctionCommand.prototype._undo = function(analysis, resolve, reject) {
  smlr.AddParamCommand.prototype._undo.call(this, analysis, resolve, reject);
};

smlr.CommandFactory.prototype.register(smlr.ApplyFunctionCommand);
