smlr.AddSeriesPlotCommand = function(props) {
  smlr.AddPlotCommand.call(this, props);

  this.cols = 12;
  this.obs = {
    color: '#424242',
    label: 'OBS',
    strokeWidth: 0,
    strokeBorderWidth: 1,
    strokePattern: Dygraph.DOTTED_LINE,
    connectSeparatedPoints: false,
    pointSize: 3,
    drawPoints: true
  };

  if (props) {
    this.seriesIndex = props.seriesIndex;
    this.cols = props.cols || this.cols;
    this.obs = props.obs || this.obs;
  }
};

smlr.AddSeriesPlotCommand.prototype = Object.create(smlr.AddPlotCommand.prototype);

smlr.AddSeriesPlotCommand.prototype.constructor = smlr.AddSeriesPlotCommand;

Object.defineProperties(smlr.AddSeriesPlotCommand.prototype, {
  'type': { value: 'series' },

  '_id': { value: 'AddSeriesPlot' },

  '_name': { value: 'Adicionar gráfico de série' },

  '_description': { get: function() {
    return 'Gráfico da série"' + this.seriesIndex + '" adicionado à análise.';
  }}
});

smlr.CommandFactory.prototype.register(smlr.AddSeriesPlotCommand);
