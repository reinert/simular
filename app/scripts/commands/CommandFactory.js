var smlr = smlr || {};

smlr.CommandFactory = function() {};

smlr.CommandFactory.prototype.create = function(serializedCommand) {
  var c = this[serializedCommand._id];

  if (!c) {
    throw new Error('Command "' + serializedCommand._id + '" does not exists');
  }

  return c(serializedCommand);
};

smlr.CommandFactory.prototype.register = function(ctor) {
  smlr.CommandFactory.prototype[ctor.prototype._id] = function(command) {
    return new ctor(command);
  };
};

smlr.Command = function(serializedCommand) {
  return smlr.CommandFactory.prototype.create(serializedCommand);
};
