smlr.RemovePlotCommand = function(props) {
  smlr.AbstractCommand.call(this);

  this.index = props.index;
  this.plot = props.plot;
};

smlr.RemovePlotCommand.prototype = Object.create(smlr.AbstractCommand.prototype);

smlr.RemovePlotCommand.prototype.constructor = smlr.RemovePlotCommand;

Object.defineProperties(smlr.RemovePlotCommand.prototype, {
  '_id': { value: 'RemovePlot' },

  '_name': { value: 'Remover gráfico' },

  '_description': { get: function() {
    return 'Gráfico removido da análise.';
  }}
});

smlr.RemovePlotCommand.prototype._do = function(analysis, resolve, reject) {
  if (this.index == null && this.plot != null) {
    this.index = analysis.plots.indexOf(this.plot);
    if (this.index === -1) {
      console.info(this.plot);
      console.log(analysis.plots);
      reject(new Error('Plot does not exists.'));
    }
    this.plot = null; // clear cache; save memory; avoid serialization problems
  }

  this.__removedPlot = analysis.splice('plots', this.index, 1)[0];

  this._callResolve(resolve);
};

smlr.RemovePlotCommand.prototype._undo = function(analysis, resolve, reject) {
  if (this.__removedPlot) {
    analysis.splice('plots', this.index, 0, this.__removedPlot);
    // analysis.push('plots', this.__removedPlot);
  }

  this._callResolve(resolve);
};

smlr.CommandFactory.prototype.register(smlr.RemovePlotCommand);
