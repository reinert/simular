smlr.AddParamCommand = function(props) {
  smlr.AbstractCommand.call(this);

  this.paramName = props.paramName;
  this.paramData = props.paramData;
};

smlr.AddParamCommand.prototype = Object.create(smlr.AbstractCommand.prototype);

smlr.AddParamCommand.prototype.constructor = smlr.AddParamCommand;

Object.defineProperties(smlr.AddParamCommand.prototype, {
  '_id': { value: 'AddParam' },

  '_name': { value: 'Adicionar parâmetro' },

  '_description': { get: function() {
      return 'Parâmetro "' + this.paramName + '" foi adicionado à análise.';
  }}
});

smlr.AddParamCommand.prototype._do = function(analysis, resolve, reject) {
  var pIdx = analysis.params[0].indexOf(this.paramName);

  if (pIdx < 0) {
    for (var i = 0; i < this.paramData.length; i++) {
      analysis.params[i + 1].push(this.paramData[i]);
    }
    if (this.paramData.obs) {
      analysis.obsParams[analysis.params[i].length-1] = this.paramData.obs;
    }
    this.__paramIndex = analysis.params[0].length;
    analysis.push('params.0', this.paramName); // NOTIFIES CHANGE
  } else {
    for (var i = 0; i < this.paramData.length; i++) {
      analysis.params[i + 1][pIdx] = this.paramData[i];
    }
    this.__paramIndex = pIdx;
  }

  this._callResolve(resolve);
};

smlr.AddParamCommand.prototype._undo = function(analysis, resolve, reject) {
  for (var i = 1; i < analysis.params.length; i++) {
    analysis.params[i].splice(this.__paramIndex, 1);
  }
  analysis.splice('params.0', this.__paramIndex, 1); // NOTIFIES CHANGE
  this.__paramIndex = null;

  this._callResolve(resolve);
};

smlr.CommandFactory.prototype.register(smlr.AddParamCommand);
