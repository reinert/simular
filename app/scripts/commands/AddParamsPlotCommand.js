smlr.AddParamsPlotCommand = function(props) {
  smlr.AddPlotCommand.call(this, props);

  this.cols = 4;
  this.obs = {
    label: 'OBS',
    color: '#424242'
  };

  if (props) {
    this.x = props.x;
    this.y = props.y;
    this.cols = props.cols || 4;
    this.obs = props.obs || this.obs;
  }
};

smlr.AddParamsPlotCommand.prototype = Object.create(smlr.AddPlotCommand.prototype);

smlr.AddParamsPlotCommand.prototype.constructor = smlr.AddParamsPlotCommand;

Object.defineProperties(smlr.AddParamsPlotCommand.prototype, {
  'type': { value: 'params' },

  '_id': { value: 'AddParamsPlot' },

  '_name': { value: 'Adicionar gráfico de parâmetros' },

  '_description': { get: function() {
    return 'Gráfico entre os parâmetros "' + this.x + '" e "' + this.y + '" adicionado à análise.';
  }}
});

smlr.AddParamsPlotCommand.prototype._do = function(analysis, resolve, reject) {
  // var x = (this.x && isNaN(this.x)) ? analysis.params[0].indexOf(this.x) : this.x;
  // var y = (this.y && isNaN(this.y)) ? analysis.params[0].indexOf(this.y) : this.y;
  if (this.x && isNaN(this.x)) this.x = analysis.params[0].indexOf(this.x);
  if (this.y && isNaN(this.y)) this.y = analysis.params[0].indexOf(this.y);

  smlr.AddPlotCommand.prototype._do.call(this, analysis, resolve, reject);
};

smlr.CommandFactory.prototype.register(smlr.AddParamsPlotCommand);
