smlr.AddFilterCommand = function(props) {
  smlr.AbstractCommand.call(this);

  // TODO: validate arguments (color must be pink, cyan or yellow)
  // if (!props.color) {
  //   throw Error('Filtro deve conter cor.');
  // }
  //if (!props.name) {
  //  throw Error('Filtro deve conter nome.');
  //}

  this.name = props.name;
  this.color = props.color || 'outline';
  this.conditions = props.conditions || [];
  this.disabled = props.disabled || false;
  this.editName = props.editName || false;
};

smlr.AddFilterCommand.prototype = Object.create(smlr.AbstractCommand.prototype);

smlr.AddFilterCommand.prototype.constructor = smlr.AddFilterCommand;

Object.defineProperties(smlr.AddFilterCommand.prototype, {
  '_id': { value: 'AddFilter' },

  '_name': { value: 'Adicionar filtro' },

  '_description': { get: function() {
      return 'Filtro "' + this.name + '" foi adicionado à análise.';
  }}
});

smlr.AddFilterCommand.prototype._do = function(analysis, resolve, reject) {
  analysis.push('filters', this.model);

  this._callResolve(resolve);
};

smlr.AddFilterCommand.prototype._undo = function(analysis, resolve, reject) {
  analysis.pop('filters');

  this._callResolve(resolve);
};

smlr.CommandFactory.prototype.register(smlr.AddFilterCommand);
