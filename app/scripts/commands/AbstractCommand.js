var smlr = smlr || {};

smlr.AbstractCommand = function() {
  // own property id
  Object.defineProperty(this, '_id', {
    value: this._id,
    enumerable: true,
    writable: false
  });
};

smlr.AbstractCommand.prototype = {
  constructor: smlr.AbstractCommand,

  get _id() {
    throw new Error('Command not implemented.');
  },

  get _name() {
    throw new Error('Command not implemented.');
  },

  get _description() {
    throw new Error('Command not implemented.');
  },

  get model() {
    // empty object that will receive copies of properties
    var clone = {};

    var i, keys = Object.keys(this);

    for (i = 0; i < keys.length; i++) {
      // copy each property into the clone
      if (keys[i] !== '_id') clone[keys[i]] = this[keys[i]];
    }

    return clone;
  },

  do: function(model, resolve, reject) {
    try {
      this._do(model, resolve, reject);
    } catch(e) {
      if (reject) {
        reject(e);
      } else {
        throw e;
      }
    }
  },

  undo: function(model, resolve, reject) {
    try {
      this._undo(model, resolve, reject);
    } catch(e) {
      if (reject) {
        reject(e);
      } else {
        throw e;
      }
    }
  },

  _do: function(model, resolve, reject) {
    throw new Error('Command not implemented.');
  },

  _undo: function(model, resolve, reject) {
    throw new Error('Command not implemented.');
  },

  _callResolve: function(resolve) {
    if (resolve) resolve(this);
  }
};
