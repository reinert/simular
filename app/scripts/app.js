(function(document) {
  'use strict';

  // Grab a reference to our auto-binding template
  // and give it some initial binding values
  // Learn more about auto-binding templates at http://goo.gl/Dx1u2g
  var app = document.querySelector('#app');

  app.isElectron = typeof isElectron !== "undefined" ? true : false;

  app.displayInstalledToast = function() {
    // Check to make sure caching is actually enabled—it won't be in the dev environment.
    if (!document.querySelector('platinum-sw-cache').disabled) {
      document.querySelector('#caching-complete').show();
    }
  };

  // Listen for template bound event to know when bindings
  // have resolved and content has been stamped to the page
  app.addEventListener('dom-change', function() {
    console.warn('dom-change');

    // FIXME: DEV MODE
    //this.onSimulationLoaded();
  });

  // See https://github.com/Polymer/polymer/issues/1381
  window.addEventListener('WebComponentsReady', function() {
    // imports are loaded and elements have been registered
    console.warn('WebComponentsReady');
    app.route = 'home';

  });

  // Close drawer after menu item is selected if drawerPanel is narrow
  app.onDataRouteClick = function() {
    var drawerPanel = document.querySelector('#paperDrawerPanel');
    if (drawerPanel.narrow) {
      drawerPanel.closeDrawer();
    }
  };

  // Scroll page to top and expand header
  app.scrollPageToTop = function() {
    // document.getElementById('mainContainer').scrollTop = 0;
  };

  app.isNotAnalysis = function(route) {
    return route != 'analysis';
  };

  app.onSimulationLoaded = function(e) {
    debugger;
    console.log(e);
    this.$.analysis.initAnalysis(e.detail.name, e.detail.params, e.detail.series, e.detail.seriesTranspose);

    document.title = e.detail.name + ' @ Simular';

    this.goTo('analysis');
  };

  app.onAnalysisLoaded = function(e) {
    this.fire('iron-signal', {name: 'analysis-loading'});

    this.$.analysis.loadAnalysis(e.detail);

    document.title = e.detail.name + ' @ Simular';

    this._loading = e.detail.plots;

    this.goTo('analysis');
  };

  app.onGraphChange = function(e) {
    if (this._loading) {
      if (e.detail === this._loading[this._loading.length-1].graph) {
        this._loading = false;
        console.info('Analysis loaded.');
        this.fire('iron-signal', {name: 'analysis-loaded'});
      }
    }
  };

  app.onToastNotification = function(e, detail) {
    var toast = document.createElement('paper-toast');
    // toast.className = detail.type || 'info';
    toast.duration = detail.duration || 3000;
    toast.text = detail.text;
    document.body.appendChild(toast);
    toast.open();
    this.async(function() {
      Polymer.dom(document.body).removeChild(toast);
    }, toast.duration + 1000);
  };

  app._goToAnalysis = function() {
    this.route = 'analysis';
  };

  app._goToHome = function() {
    this.route = 'home';
  };

  app.goTo = function(route) {
    //document.querySelector('#loader').hide();
    //Polymer.Base.transform('scale(1,0) translateZ(0)', document.querySelector('#mainToolbar .bottom-container'));
    //document.querySelector('#mainToolbar').classList.remove('tall');
    //var mainHeader = document.querySelector('#mainHeader');
    this.async(function() { this.route = route; }, 450);
  };
})(document);
