var smlr = smlr || {};

smlr.FunctionFactory = function() {};

smlr.FunctionFactory.prototype.create = function(functionId) {
  var f = this[functionId];

  if (!f) {
    throw new Error('Function "' + functionId + '" does not exists');
  }

  return f();
};

smlr.FunctionFactory.prototype._names = {};

smlr.FunctionFactory.prototype.nameOf = function(id) {
  return smlr.FunctionFactory.prototype._names[id];
};

smlr.FunctionFactory.prototype.register = function(ctor) {
  smlr.FunctionFactory.prototype._names[ctor.prototype.id] = ctor.prototype.name;
  smlr.FunctionFactory.prototype[ctor.prototype.id] = function() {
    return new ctor();
  };
};