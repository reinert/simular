smlr.LeastSquaresFunction = function() {};

smlr.LeastSquaresFunction.prototype = Object.create(smlr.AbstractFunction.prototype);

smlr.LeastSquaresFunction.prototype.constructor = smlr.LeastSquaresFunction;

smlr.LeastSquaresFunction.prototype.id = 'mq';

smlr.LeastSquaresFunction.prototype.name = 'Mínimos Quadrados';

smlr.LeastSquaresFunction.prototype._calculateSimValue = function(label, params, time, simData, obsData, t0, t1) {
  var t, i = 0, n = 0, sum = 0.0;

  for (; t = time[i], i < time.length; i++) {
    if (t === 0) continue;
    if (t > t1) break;

    if (t >= t0 && !(isNaN(simData[i]) || isNaN(obsData[i]))) {
      sum += Math.pow(simData[i] - obsData[i], 2.0);
      n  += 1;
    }
  }

  return sum / n;
};

smlr.FunctionFactory.prototype.register(smlr.LeastSquaresFunction);
