smlr.SumOfParamsFunction = function() {};

smlr.SumOfParamsFunction.prototype = Object.create(smlr.AbstractFunction.prototype);

smlr.SumOfParamsFunction.prototype.constructor = smlr.SumOfParamsFunction;

smlr.SumOfParamsFunction.prototype.id = 'sump';

smlr.SumOfParamsFunction.prototype.name = 'Soma (A+B) de Parâmetros';

smlr.SumOfParamsFunction.prototype._calculateSimValue = function(label, params, time, simData, obsData, t0, t1, pA, pB) {
  if (isNaN(params[pA]) || isNaN(params[pB]))
    return NaN;

  return params[pA] + params[pB];
};

smlr.FunctionFactory.prototype.register(smlr.SumOfParamsFunction);
