smlr.DeltaFunction = function() {};

smlr.DeltaFunction.prototype = Object.create(smlr.AbstractObsFunction.prototype);

smlr.DeltaFunction.prototype.constructor = smlr.DeltaFunction;

smlr.DeltaFunction.prototype.id = 'delta';

smlr.DeltaFunction.prototype.name = 'Delta';

smlr.DeltaFunction.prototype._calculateSimValue = function(label, params, time, simData, obsData, t0, t1) {
  var t, v0, v1, i = 0;

  for (; t = time[i], i < time.length; i++) {
    if (t === 0) continue;
    if (t > t1) break;

    if (t >= t0 && !isNaN(simData[i])) {
      if (v0 == null) {
        v0 = simData[i];
      }

      if (i+1 < time.length && time[i+1] > t1 && v1 == null) {
        v1 = simData[i];
        break;
      }
    }
  }

  if (v1 == null) {
    v1 = simData[simData.length-1];
  }

  return v1 - v0;
};

smlr.DeltaFunction.prototype._calculateObsValue = function(time, obsData, t0, t1) {
  var t, v0, v1, i = 0;

  for (; t = time[i], i < time.length; i++) {
    if (t === 0) continue;
    if (t > t1) break;

    if (t >= t0 && !isNaN(obsData[i])) {
      if (v0 == null) {
        v0 = obsData[i];
      }

      if (i+1 < time.length && time[i+1] > t1 && v1 == null) {
        v1 = obsData[i];
        break;
      }
    }
  }

  if (v1 == null) {
    v1 = obsData[obsData.length-1];
  }

  return v1 - v0;
};

smlr.FunctionFactory.prototype.register(smlr.DeltaFunction);
