smlr.RelationOfPearsonCorrelationFunction = function() {};

smlr.RelationOfPearsonCorrelationFunction.prototype = Object.create(smlr.AbstractFunction.prototype);

smlr.RelationOfPearsonCorrelationFunction.prototype.constructor = smlr.RelationOfPearsonCorrelationFunction;

smlr.RelationOfPearsonCorrelationFunction.prototype.id = 'rcp';

smlr.RelationOfPearsonCorrelationFunction.prototype.name = 'Relação da Correlação de Pearson';

smlr.RelationOfPearsonCorrelationFunction.prototype._calculateSimValue = function(label, params, time, simData, obsData, t0, t1) {
  var t, i = 0, n = 0, Sx = 0, SxySim = 0, SxyObs = 0, SySim = 0, SyObs = 0, SySim2 = 0, SyObs2 = 0;

  for (; t = time[i], i < time.length; i++) {
    if (t === 0) continue;
    if (t > t1) break;

    if (t >= t0 && !(isNaN(simData[i]) || isNaN(obsData[i]))) {
      Sx += t;

      SySim += simData[i];
      SySim2 += Math.pow(simData[i], 2);
      SxySim += t * simData[i];

      SyObs += obsData[i];
      SyObs2 += Math.pow(obsData[i], 2);
      SxyObs += t * obsData[i];

      n += 1;
    }
  }

  var rhoSim = (n*SxySim - Sx*SySim) / Math.sqrt(n*SySim2 - Math.pow(SySim, 2));
  var rhoObs = (n*SxyObs - Sx*SyObs) / Math.sqrt(n*SyObs2 - Math.pow(SyObs, 2));

  return rhoSim/rhoObs;
};

smlr.FunctionFactory.prototype.register(smlr.RelationOfPearsonCorrelationFunction);
