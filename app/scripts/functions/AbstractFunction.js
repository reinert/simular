var smlr = smlr || {};

smlr.AbstractFunction = function() {};

smlr.AbstractFunction.prototype = {
  constructor: smlr.AbstractFunction,

  _calculateSimValue: function(label, params, time, simData, obsData, t0, t1, pA, pB, opt) {
    return 0;
  },

  _getSimParams: function(paramsNames, paramsValues) {
    var obj = {};
    for (var i in paramsNames) {
      obj[paramsNames[i]] = paramsValues[i];
    }
    return obj;
  },

  execute: function(params, series, t0, t1, pA, pB, opt) {
    var newParam = [];

    if (series) {
      var time = series[0].slice(1);
      var obsData = series[series.length - 1].slice(1);
      if (!t0) t0 = time[0];
      if (!t1) t1 = time[time.length - 1];
    }

    for (var i = 1; i < params.length; i++) {
      var simParams = this._getSimParams(params[0], params[i]);
      if (series) {
        var label = series[i][0];
        var simData = series[i].slice(1);
      }

      newParam.push(this._calculateSimValue(label, simParams, time, simData, obsData, t0, t1, pA, pB, opt));
    }

    return newParam;
  }
};
