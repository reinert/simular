smlr.AbstractTwoSeriesFunction = function() {};

smlr.AbstractTwoSeriesFunction.prototype = Object.create(smlr.AbstractFunction.prototype);

smlr.AbstractTwoSeriesFunction.prototype.constructor = smlr.AbstractTwoSeriesFunction;

smlr.AbstractTwoSeriesFunction.prototype._calculateObsValue = function(time, obsData, obsDataB, t0, t1) {
  return NaN;
};

smlr.AbstractTwoSeriesFunction.prototype.execute = function(params, series, t0, t1, pA, pB, seriesB) {
  var newParam = [];

  if (series) {
    var time = series[0].slice(1);
    if (series[series.length - 1][0] === 'OBS' && seriesB[seriesB.length - 1][0] === 'OBS') {
      var obsData = series[series.length - 1].slice(1);
      var obsDataB = seriesB[seriesB.length - 1].slice(1);
    }
    if (!t0) t0 = time[0];
    if (!t1) t1 = time[time.length - 1];
  }

  for (var i = 1; i < params.length; i++) {
    var simParams = this._getSimParams(params[0], params[i]);
    if (series) {
      var label = series[i][0];
      var simData = series[i].slice(1);
      var simDataB = seriesB[i].slice(1);
    }

    newParam.push(this._calculateSimValue(label, simParams, time, simData, obsData, simDataB, obsDataB, t0, t1));
  }

  if (obsData && obsDataB) {
    newParam.obs = this._calculateObsValue(time, obsData, obsDataB, t0, t1);
  }

  return newParam;
};
