smlr.SubtractionOfParamsFunction = function() {};

smlr.SubtractionOfParamsFunction.prototype = Object.create(smlr.AbstractFunction.prototype);

smlr.SubtractionOfParamsFunction.prototype.constructor = smlr.SubtractionOfParamsFunction;

smlr.SubtractionOfParamsFunction.prototype.id = 'subp';

smlr.SubtractionOfParamsFunction.prototype.name = 'Subtração (A-B) de Parâmetros';

smlr.SubtractionOfParamsFunction.prototype._calculateSimValue = function(label, params, time, simData, obsData, t0, t1, pA, pB) {
  if (isNaN(params[pA]) || isNaN(params[pB]))
    return NaN;

  return params[pA] - params[pB];
};

smlr.FunctionFactory.prototype.register(smlr.SubtractionOfParamsFunction);
