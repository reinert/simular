smlr.AbstractObsFunction = function() {};

smlr.AbstractObsFunction.prototype = Object.create(smlr.AbstractFunction.prototype);

smlr.AbstractObsFunction.prototype.constructor = smlr.AbstractObsFunction;

smlr.AbstractObsFunction.prototype._calculateObsValue = function(time, obsData, t0, t1, pA, pB, opt) {
  return NaN;
};

smlr.AbstractObsFunction.prototype.execute = function(params, series, t0, t1, pA, pB, opt) {
  var result = smlr.AbstractFunction.prototype.execute.call(this, params, series, t0, t1, pA, pB, opt);

  if (series) {
    var time = series[0].slice(1);
    var obsData = series[series.length - 1].slice(1);
    if (!t0) t0 = time[0];
    if (!t1) t1 = time[time.length - 1];
  }

  result.obs = this._calculateObsValue(time, obsData, t0, t1, pA, pB, opt);

  return result;
};
