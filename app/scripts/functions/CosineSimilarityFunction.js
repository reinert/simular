smlr.CosineSimilarityFunction = function() {};

smlr.CosineSimilarityFunction.prototype = Object.create(smlr.AbstractFunction.prototype);

smlr.CosineSimilarityFunction.prototype.constructor = smlr.CosineSimilarityFunction;

smlr.CosineSimilarityFunction.prototype.id = 'sc';

smlr.CosineSimilarityFunction.prototype.name = 'Similaridade do Cosseno';

smlr.CosineSimilarityFunction.prototype._calculateSimValue = function(label, params, time, simData, obsData, t0, t1) {
  var t, i = 0, Sa2 = 0, Sb2 = 0, Sab = 0;

  for (; t = time[i], i < time.length; i++) {
    if (t === 0) continue;
    if (t > t1) break;

    if (t >= t0 && !(isNaN(simData[i]) || isNaN(obsData[i]))) {
      Sa2 += simData[i] * simData[i];
      Sb2 += obsData[i] * obsData[i];
      Sab += simData[i] * obsData[i];
    }
  }

  return Sab / (Math.sqrt(Sa2) * Math.sqrt(Sb2));
};

smlr.FunctionFactory.prototype.register(smlr.CosineSimilarityFunction);
