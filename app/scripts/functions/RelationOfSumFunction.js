smlr.RelationOfSumFunction = function() {};

smlr.RelationOfSumFunction.prototype = Object.create(smlr.AbstractFunction.prototype);

smlr.RelationOfSumFunction.prototype.constructor = smlr.RelationOfSumFunction;

smlr.RelationOfSumFunction.prototype.id = 'rsum';

smlr.RelationOfSumFunction.prototype.name = 'Relação da Soma';

smlr.RelationOfSumFunction.prototype._calculateSimValue = function(label, params, time, simData, obsData, t0, t1) {
  var t, i = 0, SySim = 0, SyObs = 0;

  for (; t = time[i], i < time.length; i++) {
    if (t === 0) continue;
    if (t > t1) break;

    if (t >= t0 && !isNaN(simData[i]) && !isNaN(obsData[i])) {
      SySim += simData[i];
      SyObs += obsData[i];
    }
  }

  return SySim / SyObs;
};

smlr.FunctionFactory.prototype.register(smlr.RelationOfSumFunction);
