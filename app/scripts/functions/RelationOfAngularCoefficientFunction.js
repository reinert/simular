smlr.RelationOfAngularCoefficientFunction = function() {};

smlr.RelationOfAngularCoefficientFunction.prototype = Object.create(smlr.AbstractFunction.prototype);

smlr.RelationOfAngularCoefficientFunction.prototype.constructor = smlr.RelationOfAngularCoefficientFunction;

smlr.RelationOfAngularCoefficientFunction.prototype.id = 'rca';

smlr.RelationOfAngularCoefficientFunction.prototype.name = 'Relação do Coeficiente Angular';

smlr.RelationOfAngularCoefficientFunction.prototype._calculateSimValue = function(label, params, time, simData, obsData, t0, t1) {
  var t, i = 0, n = 0, Sx = 0, SxySim = 0, SxyObs = 0, SySim = 0, SyObs = 0;

  for (; t = time[i], i < time.length; i++) {
    if (t === 0) continue;
    if (t > t1) break;

    if (t >= t0 && !(isNaN(simData[i]) || isNaN(obsData[i]))) {
      Sx += t;
      SySim += simData[i];
      SyObs += obsData[i];
      SxySim += t * simData[i];
      SxyObs += t * obsData[i];
      n += 1;
    }
  }

  var alfaSim = n*SxySim - Sx*SySim;
  var alfaObs = n*SxyObs - Sx*SyObs;

  return alfaSim/alfaObs;
};

smlr.FunctionFactory.prototype.register(smlr.RelationOfAngularCoefficientFunction);
