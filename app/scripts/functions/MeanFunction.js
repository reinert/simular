smlr.MeanFunction = function() {};

smlr.MeanFunction.prototype = Object.create(smlr.AbstractObsFunction.prototype);

smlr.MeanFunction.prototype.constructor = smlr.MeanFunction;

smlr.MeanFunction.prototype.id = 'med';

smlr.MeanFunction.prototype.name = 'Média';

smlr.MeanFunction.prototype._calculateSimValue = function(label, params, time, simData, obsData, t0, t1) {
  var t, i = 0, n = 0, Sy = 0;

  for (; t = time[i], i < time.length; i++) {
    if (t === 0) continue;
    if (t > t1) break;

    if (t >= t0 && !isNaN(simData[i])) {
      Sy += simData[i];
      n += 1;
    }
  }

  return Sy / n;
};

smlr.MeanFunction.prototype._calculateObsValue = function(time, obsData, t0, t1) {
  var t, i = 0, n = 0, Sy = 0;

  for (; t = time[i], i < time.length; i++) {
    if (t === 0) continue;
    if (t > t1) break;

    if (t >= t0 && !isNaN(obsData[i])) {
      Sy += obsData[i];
      n += 1;
    }
  }

  return Sy / n;
};

smlr.FunctionFactory.prototype.register(smlr.MeanFunction);
