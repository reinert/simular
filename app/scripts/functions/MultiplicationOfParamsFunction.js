smlr.MultiplicationOfParamsFunction = function() {};

smlr.MultiplicationOfParamsFunction.prototype = Object.create(smlr.AbstractFunction.prototype);

smlr.MultiplicationOfParamsFunction.prototype.constructor = smlr.MultiplicationOfParamsFunction;

smlr.MultiplicationOfParamsFunction.prototype.id = 'mp';

smlr.MultiplicationOfParamsFunction.prototype.name = 'Multiplicação (A*B) de Parâmetros';

smlr.MultiplicationOfParamsFunction.prototype._calculateSimValue = function(label, params, time, simData, obsData, t0, t1, pA, pB) {
  if (isNaN(params[pA]) || isNaN(params[pB]))
    return NaN;

  return params[pA] * params[pB];
};

smlr.FunctionFactory.prototype.register(smlr.MultiplicationOfParamsFunction);
