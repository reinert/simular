smlr.RelationOfParamsFunction = function() {};

smlr.RelationOfParamsFunction.prototype = Object.create(smlr.AbstractFunction.prototype);

smlr.RelationOfParamsFunction.prototype.constructor = smlr.RelationOfParamsFunction;

smlr.RelationOfParamsFunction.prototype.id = 'dp';

smlr.RelationOfParamsFunction.prototype.name = 'Divisão (A/B) de Parâmetros';

smlr.RelationOfParamsFunction.prototype._calculateSimValue = function(label, params, time, simData, obsData, t0, t1, pA, pB) {
  if (isNaN(params[pA]) || isNaN(params[pB]))
    return NaN;

  return params[pA] / params[pB];
};

smlr.FunctionFactory.prototype.register(smlr.RelationOfParamsFunction);
