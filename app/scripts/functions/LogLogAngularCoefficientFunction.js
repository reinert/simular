smlr.LogLogAngularCoefficientFunction = function() {};

smlr.LogLogAngularCoefficientFunction.prototype = Object.create(smlr.AbstractObsFunction.prototype);

smlr.LogLogAngularCoefficientFunction.prototype.constructor = smlr.LogLogAngularCoefficientFunction;

smlr.LogLogAngularCoefficientFunction.prototype.id = 'cal';

smlr.LogLogAngularCoefficientFunction.prototype.name = 'Coeficiente Angular Log-Log';

smlr.LogLogAngularCoefficientFunction.prototype._calculateSimValue = function(label, params, time, simData, obsData, t0, t1) {
  var t, i = 0, n = 0, Sx = 0, Sx2 = 0, SxySim = 0, SySim = 0;

  for (; t = time[i], i < time.length; i++) {
    if (t === 0) continue;
    if (t > t1) break;

    if (t >= t0 && !(isNaN(simData[i]) || isNaN(obsData[i]))) {
      Sx += Math.log(t);
      Sx2 += Math.log(t)*Math.log(t);
      SySim += Math.log(simData[i]);

      SxySim += Math.log(t) * Math.log(simData[i]);

      n += 1;
    }
  }

  return (n*SxySim - Sx*SySim) / (n*Sx2 - Sx*Sx);
};

smlr.LogLogAngularCoefficientFunction.prototype._calculateObsValue = function(time, obsData, t0, t1) {
  var t, i = 0, n = 0, Sx = 0, Sx2 = 0, SxySim = 0, SySim = 0;

  for (; t = time[i], i < time.length; i++) {
    if (t === 0) continue;
    if (t > t1) break;

    if (t >= t0 && !isNaN(obsData[i])) {
      Sx += Math.log(t);
      Sx2 += Math.log(t)*Math.log(t);
      SySim += Math.log(obsData[i]);

      SxySim += Math.log(t) * Math.log(obsData[i]);

      n += 1;
    }
  }

  return (n*SxySim - Sx*SySim) / (n*Sx2 - Sx*Sx);
};

smlr.FunctionFactory.prototype.register(smlr.LogLogAngularCoefficientFunction);
