smlr.AngularCoefficientFunction = function() {};

smlr.AngularCoefficientFunction.prototype = Object.create(smlr.AbstractObsFunction.prototype);

smlr.AngularCoefficientFunction.prototype.constructor = smlr.AngularCoefficientFunction;

smlr.AngularCoefficientFunction.prototype.id = 'ca';

smlr.AngularCoefficientFunction.prototype.name = 'Coeficiente Angular';

smlr.AngularCoefficientFunction.prototype._calculateSimValue = function(label, params, time, simData, obsData, t0, t1) {
  var t, i = 0, n = 0, Sx = 0, Sx2 = 0, SxySim = 0, SySim = 0;

  for (; t = time[i], i < time.length; i++) {
    if (t === 0) continue;
    if (t > t1) break;

    if (t >= t0 && !(isNaN(simData[i]) || isNaN(obsData[i]))) {
      Sx += t;
      Sx2 += t*t;
      SySim += simData[i];

      SxySim += t * simData[i];

      n += 1;
    }
  }

  return (n*SxySim - Sx*SySim) / (n*Sx2 - Sx*Sx);
};

smlr.AngularCoefficientFunction.prototype._calculateObsValue = function(time, obsData, t0, t1) {
  var t, i = 0, n = 0, Sx = 0, Sx2 = 0, SxySim = 0, SySim = 0;

  for (; t = time[i], i < time.length; i++) {
    if (t === 0) continue;
    if (t > t1) break;

    if (t >= t0 && !isNaN(obsData[i])) {
      Sx += t;
      Sx2 += t*t;
      SySim += obsData[i];

      SxySim += t * obsData[i];

      n += 1;
    }
  }

  return (n*SxySim - Sx*SySim) / (n*Sx2 - Sx*Sx);
};

smlr.FunctionFactory.prototype.register(smlr.AngularCoefficientFunction);
