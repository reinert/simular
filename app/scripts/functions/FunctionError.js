function FunctionError(value, message) {
  if (value instanceof Error) {
    this.name = value.name;
    this.message = message || value.message;
    this.stack = value.stack;
    return;
  }

  this.name = 'FunctionError';
  this.message = message || 'Erro sem descrição.';
  this.stack = (new Error()).stack;
  this.value = value;
}
FunctionError.prototype = Object.create(Error.prototype);
FunctionError.prototype.constructor = FunctionError;
