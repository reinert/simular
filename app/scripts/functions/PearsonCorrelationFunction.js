smlr.PearsonCorrelationFunction = function() {};

smlr.PearsonCorrelationFunction.prototype = Object.create(smlr.AbstractFunction.prototype);

smlr.PearsonCorrelationFunction.prototype.constructor = smlr.PearsonCorrelationFunction;

smlr.PearsonCorrelationFunction.prototype.id = 'cp';

smlr.PearsonCorrelationFunction.prototype.name = 'Correlação de Pearson';

smlr.PearsonCorrelationFunction.prototype._calculateSimValue = function(label, params, time, simData, obsData, t0, t1) {
  var t, i = 0, n = 0, Sx = 0, Sx2 = 0, Sy = 0, Sy2 = 0, Sxy = 0;

  for (; t = time[i], i < time.length; i++) {
    if (t === 0) continue;
    if (t > t1) break;

    if (t >= t0 && !(isNaN(simData[i]) || isNaN(obsData[i]))) {
      Sx += obsData[i];
      Sx2 += Math.pow(obsData[i], 2);

      Sy += simData[i];
      Sy2 += Math.pow(simData[i], 2);

      Sxy += obsData[i] * simData[i];

      n += 1;
    }
  }

  return (n*Sxy - Sx*Sy) / Math.sqrt((n*Sx2 - Math.pow(Sx, 2)) * (n*Sy2 - Math.pow(Sy, 2)));
};

smlr.FunctionFactory.prototype.register(smlr.PearsonCorrelationFunction);
