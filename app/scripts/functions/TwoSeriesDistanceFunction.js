smlr.TwoSeriesDistanceFunction = function() {};

smlr.TwoSeriesDistanceFunction.prototype = Object.create(smlr.AbstractTwoSeriesFunction.prototype);

smlr.TwoSeriesDistanceFunction.prototype.constructor = smlr.TwoSeriesDistanceFunction;

smlr.TwoSeriesDistanceFunction.prototype.id = 'tsd';

smlr.TwoSeriesDistanceFunction.prototype.name = 'Distância Entre Séries';

smlr.TwoSeriesDistanceFunction.prototype._calculateSimValue = function(label, params, time, simData, obsData, simDataB, obsDataB, t0, t1) {
  var t, i = 0, n = 0, sum = 0.0;

  for (; t = time[i], i < time.length; i++) {
    if (t === 0) continue;
    if (t > t1) break;

    if (t >= t0 && !(isNaN(simData[i]) || isNaN(simDataB[i]))) {
      sum += simData[i] - simDataB[i];
      n  += 1;
    }
  }

  return sum / n;
};

smlr.TwoSeriesDistanceFunction.prototype._calculateObsValue = function(time, obsData, obsDataB, t0, t1) {
  var t, i = 0, n = 0, sum = 0.0;

  for (; t = time[i], i < time.length; i++) {
    if (t === 0) continue;
    if (t > t1) break;

    if (t >= t0 && !(isNaN(obsData[i]) || isNaN(obsDataB[i]))) {
      sum += obsData[i] - obsDataB[i];
      n  += 1;
    }
  }

  return sum / n;
};

smlr.FunctionFactory.prototype.register(smlr.TwoSeriesDistanceFunction);
