smlr.RelativeDistanceFunction = function() {};

smlr.RelativeDistanceFunction.prototype = Object.create(smlr.AbstractFunction.prototype);

smlr.RelativeDistanceFunction.prototype.constructor = smlr.RelativeDistanceFunction;

smlr.RelativeDistanceFunction.prototype.id = 'dr';

smlr.RelativeDistanceFunction.prototype.name = 'Distância Relativa';

smlr.RelativeDistanceFunction.prototype._calculateSimValue = function(label, params, time, simData, obsData, t0, t1) {
  var t, i = 0, n = 0, sum = 0.0;

  for (; t = time[i], i < time.length; i++) {
    if (t === 0) continue;
    if (t > t1) break;

    if (t >= t0 && !(isNaN(simData[i]) || isNaN(obsData[i]))) {
      sum += simData[i] - obsData[i];
      n  += 1;
    }
  }

  return sum / n;
};

smlr.FunctionFactory.prototype.register(smlr.RelativeDistanceFunction);
