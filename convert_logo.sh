echo "converting..."

convert logo-cropped.png -fuzz 25% -fill none -opaque "rgb(249,249,249)" logo.png

# amarelo -> laranja
convert logo.png -fuzz 12% -fill "rgb(255,145,0)" -opaque "rgb(247,180,50)" logo.png
convert logo.png -fuzz 7% -fill "rgb(254,169,59)" -opaque "rgb(248,204,106)" logo.png
convert logo.png -fuzz 4% -fill "rgb(254,189,104)" -opaque "rgb(249,214,141)" logo.png
convert logo.png -fuzz 2% -fill "rgb(254,202,135)" -opaque "rgb(248,221,162)" logo.png

# verde (cima) -> amarelo
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(0,191,155)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(8,184,151)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(39,143,124)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(40,160,138)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(44,109,98)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(49,147,129)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(58,154,136)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(52,139,123)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(55,139,122)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(61,132,120)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(63,125,113)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(70,140,126)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(80,148,135)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(80,114,108)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(83,133,124)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(90,144,134)" logo.png
convert logo.png -fuzz 3% -fill "rgb(255,209,128)" -opaque "rgb(7,181,148)" logo.png
convert logo.png -fuzz 3% -fill "rgb(255,209,128)" -opaque "rgb(11,180,149)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(41,151,130)" logo.png
convert logo.png -fuzz 3% -fill "rgb(255,209,128)" -opaque "rgb(11,168,139)" logo.png
convert logo.png -fuzz 3% -fill "rgb(255,209,128)" -opaque "rgb(13,166,137)" logo.png
convert logo.png -fuzz 3% -fill "rgb(255,209,128)" -opaque "rgb(16,162,134)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(21,151,126)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(25,135,115)" logo.png
convert logo.png -fuzz 1% -fill "rgb(255,209,128)" -opaque "rgb(27,128,110)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(32,150,129)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(34,122,107)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(36,126,109)" logo.png
convert logo.png -fuzz 3% -fill "rgb(255,209,128)" -opaque "rgb(60,192,167)" logo.png
convert logo.png -fuzz 3% -fill "rgb(255,209,128)" -opaque "rgb(71,171,153)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(78,136,126)" logo.png
convert logo.png -fuzz 1% -fill "rgb(255,209,128)" -opaque "rgb(108,195,179)" logo.png
convert logo.png -fuzz 1% -fill "rgb(255,209,128)" -opaque "rgb(109,186,173)" logo.png
convert logo.png -fuzz 2% -fill "rgb(84,83,84)" -opaque "rgb(36,134,115)" logo.png
convert logo.png -fuzz 1% -fill "rgb(84,83,84)" -opaque "rgb(36,115,101)" logo.png
convert logo.png -fuzz 2% -fill "rgb(84,83,84)" -opaque "rgb(38,121,106)" logo.png
convert logo.png -fuzz 1% -fill "rgb(84,83,84)" -opaque "rgb(42,122,107)" logo.png
convert logo.png -fuzz 3% -fill "rgb(255,209,128)" -opaque "rgb(14,195,162)" logo.png
convert logo.png -fuzz 3% -fill "rgb(255,209,128)" -opaque "rgb(21,178,148)" logo.png
convert logo.png -fuzz 3% -fill "rgb(255,209,128)" -opaque "rgb(33,200,168)" logo.png
convert logo.png -fuzz 3% -fill "rgb(255,209,128)" -opaque "rgb(34,179,152)" logo.png
convert logo.png -fuzz 3% -fill "rgb(255,209,128)" -opaque "rgb(40,176,151)" logo.png
convert logo.png -fuzz 3% -fill "rgb(255,209,128)" -opaque "rgb(50,204,174)" logo.png
convert logo.png -fuzz 3% -fill "rgb(255,209,128)" -opaque "rgb(52,192,166)" logo.png
convert logo.png -fuzz 3% -fill "rgb(255,209,128)" -opaque "rgb(53,179,155)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(64,206,179)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(77,210,184)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(79,209,183)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(88,212,187)" logo.png
convert logo.png -fuzz 2% -fill "rgb(255,209,128)" -opaque "rgb(91,212,189)" logo.png
convert logo.png -fuzz 2% -fill "rgb(254,210,133)" -opaque "rgb(97,213,191)" logo.png
convert logo.png -fuzz 2% -fill "rgb(254,210,133)" -opaque "rgb(101,215,192)" logo.png
convert logo.png -fuzz 1% -fill "rgb(254,210,133)" -opaque "rgb(109,216,197)" logo.png
convert logo.png -fuzz 1% -fill "rgb(254,210,133)" -opaque "rgb(114,218,198)" logo.png
convert logo.png -fuzz 1% -fill "rgb(254,210,133)" -opaque "rgb(120,219,201)" logo.png
convert logo.png -fuzz 1% -fill "rgb(254,210,133)" -opaque "rgb(121,219,200)" logo.png
convert logo.png -fuzz 1% -fill "rgb(253,225,178)" -opaque "rgb(123,159,152)" logo.png
convert logo.png -fuzz 1% -fill "rgb(253,225,178)" -opaque "rgb(125,220,202)" logo.png
convert logo.png -fuzz 1% -fill "rgb(253,225,178)" -opaque "rgb(130,222,204)" logo.png
convert logo.png -fuzz 1% -fill "rgb(253,225,178)" -opaque "rgb(132,222,206)" logo.png
convert logo.png -fuzz 1% -fill "rgb(253,225,178)" -opaque "rgb(134,221,206)" logo.png
convert logo.png -fuzz 1% -fill "rgb(253,225,178)" -opaque "rgb(138,223,207)" logo.png
convert logo.png -fuzz 1% -fill "rgb(253,225,178)" -opaque "rgb(140,223,208)" logo.png
convert logo.png -fuzz 1% -fill "rgb(253,225,178)" -opaque "rgb(136,214,200)" logo.png
convert logo.png -fuzz 1% -fill "rgb(251,238,212)" -opaque "rgb(143,224,209)" logo.png
convert logo.png -fuzz 1% -fill "rgb(251,238,212)" -opaque "rgb(97,204,186)" logo.png
convert logo.png -fuzz 1% -fill "rgb(251,238,212)" -opaque "rgb(149,224,211)" logo.png
convert logo.png -fuzz 1% -fill "rgb(251,238,212)" -opaque "rgb(152,227,213)" logo.png
convert logo.png -fuzz 1% -fill "rgb(251,238,212)" -opaque "rgb(157,228,214)" logo.png
convert logo.png -fill "rgb(251,238,212)" -opaque "rgb(157,226,214)" logo.png
convert logo.png -fuzz 1% -fill "rgb(251,238,212)" -opaque "rgb(160,228,215)" logo.png
convert logo.png -fuzz 1% -fill "rgb(251,238,212)" -opaque "rgb(162,210,201)" logo.png
convert logo.png -fuzz 1% -fill "rgb(251,238,212)" -opaque "rgb(164,229,217)" logo.png
convert logo.png -fuzz 1% -fill "rgb(250,244,231)" -opaque "rgb(168,230,219)" logo.png
convert logo.png -fuzz 1% -fill "rgb(250,244,231)" -opaque "rgb(172,232,220)" logo.png
convert logo.png -fill "rgb(250,244,231)" -opaque "rgb(174,231,220)" logo.png
convert logo.png -fuzz 1% -fill "rgb(250,244,231)" -opaque "rgb(179,233,223)" logo.png
convert logo.png -fuzz 1% -fill "rgb(250,244,231)" -opaque "rgb(186,234,225)" logo.png
convert logo.png -fuzz 1% -fill "rgb(250,244,231)" -opaque "rgb(190,235,227)" logo.png
convert logo.png -fuzz 1% -fill "rgb(250,244,231)" -opaque "rgb(191,236,227)" logo.png
convert logo.png -fuzz 2% -fill none -opaque "rgb(182,219,214)" logo.png
convert logo.png -fuzz 3% -fill none -opaque "rgb(196,237,229)" logo.png
convert logo.png -fuzz 5% -fill none -opaque "rgb(219,242,238)" logo.png

# verde (baixo) -> verde claro
convert logo.png -fuzz 25% -fill "rgb(210,218,222)" -opaque "rgb(2,177,165)" logo.png
convert logo.png -fuzz 13% -fill "rgb(210,218,222)" -opaque "rgb(121,211,206)" logo.png
convert logo.png -fuzz 10% -fill "rgb(222,228,231)" -opaque "rgb(165,225,221)" logo.png
convert logo.png -fuzz 3% -fill "rgb(238,241,242)" -opaque "rgb(213,239,238)" logo.png

# vermelho -> verde escuro
convert logo.png -fuzz 15.5% -fill "rgb(96,125,139)" -opaque "rgb(188,54,37)" logo.png
convert logo.png -fuzz 12% -fill "rgb(108,135,148)" -opaque "rgb(206,103,98)" logo.png
convert logo.png -fuzz 8% -fill "rgb(108,135,148)" -opaque "rgb(217,141,138)" logo.png
convert logo.png -fuzz 9% -fill "rgb(108,135,148)" -opaque "rgb(219,150,148)" logo.png
convert logo.png -fuzz 7% -fill "rgb(151,169,179)" -opaque "rgb(229,188,188)" logo.png
convert logo.png -fuzz 4% -fill "rgb(179,193,200)" -opaque "rgb(237,211,211)" logo.png
convert logo.png -fuzz 2% -fill "rgb(179,193,200)" -opaque "rgb(241, 224, 225)" logo.png

convert logo.png -resize 152x -unsharp 0.25x0.25+8+0.065 -dither FloydSteinberg "app/images/touch/apple-touch-icon.png"
convert logo.png -resize 384x -unsharp 0.25x0.25+8+0.065 -dither FloydSteinberg "app/images/touch/chrome-splashscreen-icon-384x384.png"
convert logo.png -resize 192x -unsharp 0.25x0.25+8+0.065 -dither FloydSteinberg "app/images/touch/chrome-touch-icon-192x192.png"
convert logo.png -resize 128x -unsharp 0.25x0.25+8+0.065 -dither FloydSteinberg "app/images/touch/icon-128x128.png"
convert logo.png -resize 144x -unsharp 0.25x0.25+8+0.065 -dither FloydSteinberg "app/images/touch/ms-icon-144x144.png"
convert logo.png -resize 144x -unsharp 0.25x0.25+8+0.065 -dither FloydSteinberg "app/images/touch/ms-touch-icon-144x144-precomposed.png"

convert logo.png -define icon:auto-resize=256 "build/icon.ico"

echo "finished!"